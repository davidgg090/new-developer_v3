import pytest
from main import find_matching_pair


# Happy path tests
@pytest.mark.parametrize("numbers, target_sum, expected", [
    pytest.param([1, 2, 3, 4, 5], 9, (4, 5), id="pair_at_end"),
    pytest.param([2, 0, 2, 3, 4], 2, (2, 0), id="pair_with_zero"),
    pytest.param([5, 4, 3, 2, 2], 7, (5, 2), id="pair_with_negatives"),
    pytest.param([1, 1, 3, 3], 4, (1, 3), id="pair_with_duplicates"),
    pytest.param([1, 2, 3, 4, 5], 3, (1, 2), id="pair_at_start"),
], ids=str)
def test_find_matching_pair_happy_path(numbers, target_sum, expected):
    # Act
    result = find_matching_pair(numbers, target_sum)

    # Assert
    assert result == expected, f"Expected {expected}, got {result}"


# Edge cases
@pytest.mark.parametrize("numbers, target_sum, expected", [
    pytest.param([], 0, None, id="empty_list"),
    pytest.param([1], 2, None, id="single_element_list"),
    pytest.param([1, 2], 4, None, id="no_matching_pair"),
    pytest.param([1, 2, 3, 4, 5], 10, None, id="target_sum_too_high"),
    pytest.param([1, 2, 3, 4, 5], 1, None, id="target_sum_too_low"),
], ids=str)
def test_find_matching_pair_edge_cases(numbers, target_sum, expected):
    # Act
    result = find_matching_pair(numbers, target_sum)

    # Assert
    assert result == expected, f"Expected {expected}, got {result}"


# Error cases
@pytest.mark.parametrize("numbers, target_sum, expected_exception", [
    pytest.param("not_a_list", 0, TypeError, id="non_list_input"),
    pytest.param([1, 2, 3], "not_an_int", TypeError, id="non_int_target_sum"),
], ids=str)
def test_find_matching_pair_error_cases(numbers, target_sum, expected_exception):
    # Act & Assert
    with pytest.raises(expected_exception):
        find_matching_pair(numbers, target_sum)

FROM python:3.9-slim

WORKDIR /app

COPY requirements.txt .


RUN pip install --no-cache-dir -r requirements.txt


COPY main.py .
COPY test_main.py .

RUN python -m pytest test_main.py


CMD ["python", "./main.py"]

# Answer File


## Answer 1

### RTFM and LMGTFY Usage

**Last Used RTFM:** Working on the configuration of a web system I need to configure the connection to the Database in this case using Postgresql with the ORM SQLAlchemy, as I progressed I found several types of errors, so I decided to review the website to improve the connection syntax and also optimize and configure as the official documentation suggests.

**Last Used LMGTFY:** Working on an ETL process locally on my PC, I was processing a fairly large file with a .csv extension; At one point when I was making modifications to my code I had the error: MemoryError which seemed quite strange to me because the specifications of my PC in that regard were good, I searched the official Pandas documentation to see if there was anything related, I didn't see much who could provide me with a clear solution to my problem, which is why I investigated further on the Stack Overflow website; There I found the solution to my problem, which was to divide the file into smaller pieces to process it correctly, ensuring optimization of system resources and eliminating the error I was having.


**Operating System:** MacOS, Linux

**Languages:** Python, JavaScript, C++


## Answer 2

```python
def build_and_test():
  """
  Simulates a CI pipeline that builds and tests the code.

  This function does not actually perform the actions, but represents 
  the typical steps involved in a CI pipeline.

  Returns:
    None
  """

  print("Build the code.")
  # Simulate the build (replace with your actual build command)
  # os.system("make")  # Assuming a Makefile is used

  print("Run unit tests.")
  # Simulate the unit tests (replace with your actual test command)
  # os.system("python test.py")

  print("Run integration tests.")
  # Simulate the integration tests (replace with your actual command)

  print("Run system tests.")
  # Simulate the system tests (replace with your actual command)

  print("All tests passed.")

  print("Formatting the code.")
  # Simulate the code formatting (replace with your actual command)

  print("Linting the code.")
  # Simulate the code linting (replace with your actual command)

  print("All checks passed.")

  return None
```

## Answer 3

# Developer Portfolio

## Introduction

Software Engineer | Proficient in AWS, GCP & Azure | Driven by a Passion for Coding and Tech Innovation

## Portfolio Projects

### Project: REST API Restaurant Reservation System
- Description: This project is a restaurant reservation system implemented with FastAPI, designed to demonstrate the creation of RESTful APIs using this modern and fast Python framework. It allows users to create, read, update and delete reservations, as well as manage users and tables in the restaurant.
- Technologies Used: Python, FastAPI, SQLAlchemy, PostgreSQL, Docker, Swagger, Pydantic, JWT, Bcrypt
- Link to GitHub Repository: [REST API Restaurant Reservation System](https://github.com/davidgg090/FastReserveAPI)


## Answer 4

Function to find a matching pair of numbers in a sorted list that add up to a given target sum.

Assumptions:
- The input collection is a list of integers.
- The list is sorted in ascending order.

Args:
- numbers (list[int]): A list of integers in ascending order.
- target_sum (int): The target sum to find a matching pair for.

Returns:
- tuple[int, int] | None: A tuple containing the matching pair (a, b) if found, otherwise None.

See the implementation in main.py



## Answer 5
Estimado martin@mendosadelsol.com

Para saber que lleguemos a esta parte, por favor, agregue las siguientes líneas al final del archivo README.md:

Gracia,

Y esperemos que le haya gustado!

def print_rtfm_lmgtfy_info():
    """
    Prints information about the usage of RTFM and LMGTFY.

    Returns:
        None
    """

    text = """RTFM and LMGTFY Usage
    Last Used RTFM: Working on the configuration of a web system I need to configure the connection to the Database 
    in this case using Postgresql with the ORM SQLAlchemy, as I progressed I found several types of errors, 
    so I decided to review the website to improve the connection syntax and also optimize and configure as the 
    official documentation suggests.    
    Last Used LMGTFY: Working on an ETL process locally on my PC, I was processing a fairly large file with a .csv 
    extension; At one point when I was making modifications to my code I had the error: MemoryError which seemed 
    quite strange to me because the specifications of my PC in that regard were good, I searched the official Pandas 
    documentation to see if there was anything related, I didn't see much who could provide me with a clear solution 
    to my problem, which is why I investigated further on the Stack Overflow website; There I found the solution to 
    my problem, which was to divide the file into smaller pieces to process it correctly, ensuring optimization of 
    system resources and eliminating the error I was having.
    Operating System: MacOS, Linux
    Languages: Python, JavaScript, C++
    """
    print(text)


def build_and_test():
    """
      Simulates a CI pipeline that builds and tests the code.
    Returns:
        None
    Examples:
        build_and_test()
    """
    print("Build the code.")
    # Simulate the build
    print("Run unit tests.")
    # Simulate the unit tests
    print("Run integration tests.")
    # Simulate the integration tests
    print("Run system tests.")
    # Simulate the system tests
    print("All tests passed.")
    print("Formatting the code.")
    # Simulate the code formatting
    print("Linting the code.")
    # Simulate the code linting
    print("All checks passed.")
    return None


def print_developer_portfolio():
    """
    Prints the developer's portfolio, including an introduction and portfolio projects.

    Returns:
        None
    """

    text = """
    # Developer Portfolio    
    ## Introduction
    Software Engineer | Proficient in AWS, GCP & Azure | Driven by a Passion for Coding and Tech Innovation
    ## Portfolio Projects
    ### Project: REST API Restaurant Reservation System - Description: This project is a restaurant reservation 
    system implemented with FastAPI, designed to demonstrate the creation of RESTful APIs using this modern and fast 
    Python framework. It allows users to create, read, update and delete reservations, as well as manage users and 
    tables in the restaurant. - Technologies Used: Python, FastAPI, SQLAlchemy, PostgreSQL, Docker, Swagger, 
    Pydantic, JWT, Bcrypt - Link to GitHub Repository: [REST API Restaurant Reservation System](
    https://github.com/davidgg090/FastReserveAPI)"""
    print(text)


def find_matching_pair(numbers, target_sum):
    """
    Finds a pair of numbers in a sorted list that add up to a given target sum.

    Assumptions:
     - The input collection is a list of integers.
     - The list is sorted in ascending order.
    Args:
        numbers (list[int]): A list of integers in ascending order.
        target_sum (int): The target sum to find a matching pair for.

    Returns:
        tuple[int, int] | None: A tuple containing the matching pair (a, b)
                                if found, otherwise None.
    """

    left_index = 0
    right_index = len(numbers) - 1

    while left_index < right_index:
        current_sum = numbers[left_index] + numbers[right_index]
        if current_sum == target_sum:
            print("Pair found:", numbers[left_index], numbers[right_index])
            return numbers[left_index], numbers[right_index]
        elif current_sum < target_sum:
            left_index += 1
        else:
            right_index -= 1
    print("No matching pair found.")
    return None


def main():
    print("Function to print RTFM and LMGTFY information.")
    print_rtfm_lmgtfy_info()
    print("Function to simulate a CI pipeline.")
    build_and_test()
    print("Function to print the developer's portfolio.")
    print_developer_portfolio()
    numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    target_sum = 10
    print("Function to find a matching pair of numbers.")
    find_matching_pair(numbers, target_sum)


if __name__ == "__main__":
    main()
